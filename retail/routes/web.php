<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

 Auth::routes();
Route::get('/reg','adminController@register')->name('reg');
Route::get('/home', 'HomeController@index')->name('home');


Route::middleware(['auth','verified'])->group(function () {

		Route::resource('productos','ProductosController');
		Route::resource('ventas','ventasController');
		Route::resource('clientes','clientesController');
		
		//ALTA A PROVEDORES-----------------
		Route::get('/storeProvedor','adminController@createProvedor')->name('storeProvedor');
		Route::get('/storeCategoria','adminController@createCategoria')->name('storeCategoria');
		Route::get('/storeLinea','adminController@createLinea')->name('storeLinea');
		Route::get('/storeMarca','adminController@createMarca')->name('storeMarca');
		Route::get('/storeProductos','ProductosController@store')->name('storeProducto');

		Route::post('/uploadImg','adminController@uploadImg');
		Route::post('/buscarProducto','adminController@buscarProducto')->name('buscarProducto');
		Route::post('/buscarXcodigo','adminController@buscarXcodigo')->name('buscarXcodigo');

		Route::post('productos-import-loadExcel','ProductosController@loadExcel')->name('productos.import.loadExcel');
		// Route::post('import-list-excel', 'UserController@importExcel')->name('users.import.excel');
		// Route::post('productos/delete','ProductosController@destroy')->name('deleteProductos');
		Route::get('productos-search','ProductosController@search')->name('productos.search');
		Route::get('clientes-search','clientesController@search')->name('clientes.search');
		
		Route::post('/verProducto','ProductosController@verProducto')->name('verProducto');
		Route::post("/vercliente",'clientesController@verCliente')->name('vercliente');

		Route::post("productos-actualizar",'ProductosController@actualizar')->name('actualizar');
		Route::post("actualizar-cliente",'clientesController@actualizar')->name('actualizar-cliente');
		Route::get('/clientes','clientesController@index')->name('clientes.index');
});