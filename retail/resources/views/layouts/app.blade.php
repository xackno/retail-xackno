


<!DOCTYPE html>
<html lang="en">
  <head>
    <link rel="shortcut icon" href="public/img/new-logo.png" type="image/x-icon">
    <meta name="description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@pratikborsadiya">
    <meta property="twitter:creator" content="@pratikborsadiya">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Vali Admin">
    <meta property="og:title" content="Vali - Free Bootstrap 4 admin theme">
    <meta property="og:url" content="http://pratikborsadiya.in/blog/vali-admin">
    <meta property="og:image" content="http://pratikborsadiya.in/blog/vali-admin/hero-social.png">
    <meta property="og:description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <title>Retail LATAM</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="public/css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


    <meta name="csrf-token" content="{{ csrf_token() }}"> 

  </head>












  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a tabindex="-1" class="app-header__logo" href="{{ url('/home') }}">{{config('app.name')}} </a>
      <!-- Sidebar toggle button-->
      <a class="app-sidebar__toggle" tabindex="-1" id="btn_toggle" data-toggle="sidebar" ></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
       <!--  <li class="app-search">
          <form>
            <input class="app-search__input" type="search" placeholder="buscar">
          <button class="app-search__button"><i class="fa fa-search"></i></button>
          </form>
          
        </li> -->
        <!--Notification Menu-->
        <li class="dropdown"><a tabindex="-1" class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Show notifications"><i class="fa fa-bell-o fa-lg"></i></a>
          <ul class="app-notification dropdown-menu dropdown-menu-right">
            <li class="app-notification__title">Tiene 4 nuevas notificaciones</li>
            <div class="app-notification__content">
             
              
              <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-success"></i><i class="fa fa-money fa-stack-1x fa-inverse"></i></span></span>
                  <div>
                    <p class="app-notification__message">Transaction complete</p>
                    <p class="app-notification__meta">2 days ago</p>
                  </div></a></li>

            </div>
            <li class="app-notification__footer"><a href="#">Ver todas las notificaciones.</a></li>
          </ul>
        </li>
        <!-- User Menu-->
        <li class="dropdown"><a  tabindex="-1" class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li tabindex="-1"><a class="dropdown-item" href=""><i class="fa fa-cog fa-lg"></i> Configuración</a></li>
            <li tabindex="-1"><a class="dropdown-item" href=""><i class="fa fa-user fa-lg"></i> Perfil</a></li>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i>
                    {{ __('Salir') }}  
                </a>

                <form id="logout-form" tabindex="-1" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            


          </ul>
        </li>
      </ul>
    </header>

    <aside class="app-sidebar" id="aside">
      <div class="app-sidebar__user"><!-- <img class="app-sidebar__user-avatar" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image"> -->
        <img src="public/img/new-logo.png" width="30px" class="app-sidebar__user-avatar" alt="User Image">
        <div>
          <p class="app-sidebar__user-name">
            @if (Route::has('login'))
              @auth
                {{ Auth::user()->name }}

            </p>
          <p class="app-sidebar__user-designation"><span class="text-secondary">Type: </span>{{ Auth::user()->type }}</p>
          @endauth
            @endif
        </div>
      </div>
      <ul class="app-menu">
        <!-- app-menu__item active -->
        <li><a tabindex="-1" class="app-menu__item" href="{{url('/home')}}"><i class="app-menu__icon fa fa-home"></i><span class="app-menu__label">Inicio</span></a></li>

        <li><a tabindex="-1" class="app-menu__item" href="{{route('ventas.index')}}"><i class="app-menu__icon   fa fa-shopping-cart"></i><span class="app-menu__label">Ventas</span></a></li>
    
        <li><a tabindex="-1" class="app-menu__item" href="{{ route('productos.index') }}"><i class="app-menu__icon fa fa-shopping-basket"></i><span class="app-menu__label">Productos</span></a></li>

        <li><a tabindex="-1" class="app-menu__item" href="{{route('clientes.index')}}"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">Clientes</span></a></li>

        <!-- <li><a class="app-menu__item" href=""><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">UsuariosAPP</span></a></li> -->

        <li><a tabindex="-1" class="app-menu__item" href=""><i class="app-menu__icon fa fa-clipboard"></i><span class="app-menu__label">Pedidos</span></a></li>
        
        <li><a tabindex="-1" class="app-menu__item" href=""><i class="app-menu__icon fa fa fa-area-chart"></i><span class="app-menu__label">Historial</span></a></li>
  
      </ul>
    </aside>

      <br><br><br>
            
        
        <div class="container" style="padding: 0px;margin-left: 3.5%;">
          @yield('content')
          
        </div>
        


    
  </body>


<!-- Essential javascripts for application to work-->
    <script src="public/js/jquery-3.2.1.min.js"></script>
    <script src="public/js/popper.min.js"></script>
    <script src="public/js/bootstrap.min.js"></script>
    <script src="public/js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="public/js/plugins/pace.min.js"></script>
    <!-- Page specific javascripts-->
    <script type="text/javascript" src="public/js/plugins/chart.js"></script>

    <script>
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
    </script>
    @yield('script')
    
    <!-- <script type="text/javascript" src="public/js/productos.js"></script> -->

</html>






