@extends('layouts.app')

@section('content')

	        
  <div class="card" style="width: 114%">
    <div class="card-body">
      <div class="table-responsive" style="height: 450px">
		    <table class="table table-striped table-bordered" id="tabla_venta">
		    	<thead class="table-dark ">
            <tr>   
             <th class="d-none">id</th>
             <th>Cantidad</th>
			       <th>Unidad</th>
			       <th>Descripción</th>
			       <th >precio</th>
			       <th>Importe</th> 
             <th><i class="fa fa-trash-o"></i></th>   
	          </tr>
	        </thead>
          <tbody id="tbody_venta">
              <tr style="font-size: 0%;margin: 0px;padding: 0px;border:0px" class="d-none">
                <td class="d-none" ></td>
                <td id="start0" ></td>
              </tr>
          </tbody>
	                
		  </table>	  
		</div>
   </div>
  </div>

<script type="text/javascript">
    var start = document.getElementById('start0');
    start.focus();
    start.style.backgroundColor = 'green';
    start.style.color = 'white';

    function dotheneedful(sibling) {
      if (sibling != null) {
        start.focus();
        start.style.backgroundColor = '';
        start.style.color = '';
        sibling.focus();
        sibling.style.backgroundColor = 'green';
        sibling.style.color = 'white';
        start = sibling;
      }
    }
    document.onkeydown = checkKey;

    function checkKey(e) {
      e = e || window.event;
      if (e.keyCode == '38') {
      //up arrow
        var idx = start.cellIndex;
        var nextrow = start.parentElement.previousElementSibling;
        if (nextrow != null) {
          var sibling = nextrow.cells[idx];
          dotheneedful(sibling);
        }
      } else if (e.keyCode == '40') {
        //down arrow
        var idx = start.cellIndex;
        var nextrow = start.parentElement.nextElementSibling;
          if (nextrow != null) {
            var sibling = nextrow.cells[idx];
            dotheneedful(sibling);
          }
      }
    }
</script>

<div id="pie"  ><br>
    <input type="text" name="findProducto" id="findProducto"  class="form-control" placeholder="Buscar producto" autocomplete="off" autofocus="true">
    
    <button tabindex="-1" class="btn btn-success" onclick="buscarPro();" style="padding: 2px"><i class="fa fa-search" style="color: white;font-size: 215%;padding: 0px;"></i></button>

    <button tabindex="-1" class="btn btn-secondary func" id="btn_devoluciones"> <i class="zmdi zmdi-shopping-cart" style="color: white;"></i>Dev</button>
    <button tabindex="-1" class="btn btn-dark func" id="f9"><i class="zmdi  zmdi-shopping-cart-plus" style="color: white;"></i>F9</button>

    <button tabindex="-1" class="btn btn-primary func" id="f7"><i class="fa fa-power" style="color: white;"></i>F7</button>
    <button tabindex="-1" class="btn btn-warning func"  id="f4">F4</button>
    <div id="totaldiv">
         <h1 class="total">TOTAL: $</h1><h1 id="totalpagar" class="total">0.00</h1><h1 class="total">.  MXN</h1>
         <h5 id="ultimoCambio" class="text-danger"></h5> 
    </div>
    <br>
    <div id="info">
        <p>Usuario:</p> <p id="usuario"> </p> 
        <p> fecha:</p><p id="fecha"></p> <p>Hora:</p><p id="hora"></p><p>Turno:</p><p id="turno"></p>
    </div>
</div>

<style type="text/css">
	/*****************FOOTER**************************/
   .func{font-size: 90%}
    .total{display: inline-block;color:black;font-size:200%;}
    #totaldiv{float: right;}

    #pie{
        background-color:#dc3545;
        bottom:0;
        position: fixed ;
        width: 96.5%;   
        font-size: 80%;

    }
    #findProducto{width: 25%;float: left;color:blue;font-family:"Arial BLack"; }
    #info{color: white;float: left;width: 100%;font-size: 100%}
    #info p{display: inline-block;margin-left: 5px}

  #tb_busqueda tr:focus{
    background-color:#28a745;
    color: white;
  }
  #tabla_venta tr td:focus{
    background-color:#28a745;
    color: white;
  }

</style>
	      

	    

<!--window modal ######modal busqueda################-->
  <div class="modal fullscreen-modal fade" id="miModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" style="height: 500px">
        <div class="modal-body">
        <div class="table_responsive" style="overflow-x: scroll;overflow-y: scroll;height: 450px">
          <table id="tabla_busqueda" editabled="" class="table table-striped table-bordered" >
               <thead class="table-dark">
                   <tr>
                      <th>Id</th>
                      <th>Clave</th>
                      <th class="">Unidad</th>
                      <th style="width: 50%">Descripción</th>
                      <th>$ Precio</th>
                      <th>Exist</th>
                      <th style="width: 30%"> <i class="fa fa-map-marker"></i> </th>
                      <th><i class="fa fa-picture-o"></i></th>
                   </tr>
               </thead>
               <tbody id="tb_busqueda" tabindex="">

               </tbody>
           </table>   
        </div>
        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal ver imgs################-->
  <div class="modal fullscreen-modal fade" id="modal_imagenes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog " role="document">
      <div class="modal-content" >
        <div class="modal-header text-dark" style="background-color: #ffc107;" >
          <h1> imagenes <i class="app-menu__icon  fa fa-picture-o"></i></h1>
        </div>
        <div class="modal-body" style="text-align: center">

        </div>
      </div>
    </div>
  </div>
	



@endsection

@section('script')
<script type="text/javascript">
  $("#btn_toggle").trigger("click");

  var total=0;
  var clickImg=0;
  var ContTr=1;

//########################fecha arriba y abajo en la tabla de venta#####################
  $(document).keydown(
    function(e)
    {    
        if (e.keyCode == 40) {      
            $(".move:focus").next().focus();

        }
        if (e.keyCode == 38) {      
            $(".move:focus").prev().focus();

        }
    }
  );

//_______________________________click a  la imagen_____________________________________
  $("#tabla_busqueda tbody").on('click','.td_imagen',function(e){
    clickImg=1;
    var imagenes=$(this).find("span").html();
    if (imagenes=="ninguno") {
        $("#modal_imagenes .modal-body").append("<div style='width:100%'><h4 class='text-info'>Sin imagenes</h4></div>");
    }else{
        imagenes=imagenes.split(",");
        $("#modal_imagenes .modal-body").html("");
        for(var x=0;x<imagenes.length;x++){
          $("#modal_imagenes .modal-body").append("<div style='width:100%'><img src='public/img/"+imagenes[x]+"' style='width:100%;height:auto;'></div>");
          }
      }
      $("#modal_imagenes").modal('show'); 
  });
//################FUNCIO DE BUSCAR PRODUCTO#############################################
  function buscarPro(){
    $("#tabla_busqueda tbody").html("");
        $('#miModal').modal('show');
        var producto=$("#findProducto").val();
        $.ajax({
          url:'{{route("buscarProducto")}}',
          type:'POST',
          dataType:'json',
          data:{ producto:producto}
            }).done(function(e){
              if (e=="") {
               $("#tabla_busqueda tbody").append("<tr class='text-danger'> <td  colspan='8'><b>No se encontró registro!!</b></td></tr>");
              }else{
                for(var x=0;x<e.length;x++){
                $("#tabla_busqueda tbody").append("<tr tabindex='1' class='move'><td width='1px' >"+e[x].id+"</td><td>"+e[x].clave+"</td><td class=''>"+e[x].unidad+"</td><td>"+e[x].descripcion_articulo+"</td><td>"+e[x].precio_venta+"</td><td>"+e[x].existencia+"</td> <td>"+e[x].ubicacion_producto+"</td><td class='td_imagen'> <i class='fa fa-eye'></i> <span class='d-none'>"+e[x].fotos+"</span></td></tr>");

                

               }//fin for
              }
              
              });//fin done 
            $("#tabla_busqueda tbody tr[tabindex=0]").focus(); 
  }

function seleccionarProducto(id_art,unidad,descripcion,precio0,existencia){

  if (existencia<1) {
        var confirmar=confirm("¡¡¡NO HAY PRODUCTO EN EXISTENCIA!!! \n ¿DESEA AGREGARLO A LA TABLA DE VENTAS?\n \n");
        if (confirmar==true) {
          total=total+parseFloat(precio0);
        //#############para validar por si ya existe solo cambiar la cantidad########
          $("#tabla_venta #tbody_venta").find("tr td:first-child").each(function(){
                  if (id_art==$(this).html()) {
                    yaExiste=1;
                    $(this).siblings("td").eq(0).html(parseFloat($(this).siblings("td").eq(0).html())+1);
                    var importe=parseFloat($(this).siblings("td").eq(0).html())*parseFloat($(this).siblings("td").eq(3).html());
                    importe=importe.toFixed(2);
                    $(this).siblings("td").eq(4).html(importe);
                  }
            });//fin each
                    
            if (yaExiste==0) {
              if (true) {//$("#tipo_usuario_log").val()=="admin"
              $("#tabla_venta tbody").append("<tr ><td class='d-none'>"+id_art+"</td> <td contenteditable='true' id='start"+ContTr+"' >1</td> <td>"+unidad+"</td><td>"+descripcion+"</td><td>"+precio0+"</td> <td>"+precio0+"</td><td><i class='fa fa-trash-o' onclick='eliminarPRO();' style='color:red'></i></td></tr>");
              ContTr++;
              }
            //   else{
            //     $("#tabla_venta tbody").append("<tr><td class='d-none'>"+id_art+"</td> <td contenteditable='true'>1</td> <td class=''>"+unidad+"</td><td>"+descripcion+"</td><td contenteditable='true' "+ClaseOcultar+">0</td>  <td>"+precio0+"</td> <td class='d-none'>"+existencia+"</td> <td>"+precio0+"</td><td><i class='fa fa-trash-o' onclick='eliminarPRO();' style='color:red'></i></td> </tr>");

            // }//fin else   
            }
            yaExiste=0;
          $('#miModal').modal('hide')
          $("#findProducto").val("");
          $("#findProducto").focus();
        //agregar total
        total=0;
        $("#tabla_venta #tbody_venta").find("tr td:nth-child(6)").each(function(){
                total+=parseFloat($(this).html());
            });
            total=total.toFixed(2);
        $("#totalpagar").html(total);
        }else{
          alert("NO SE AGREGÓ SU PRODUCTO");
        }

      }//fin if
      if(existencia>0){
        total=total+parseFloat(precio0);
        //#############para validar por si ya existe solo cambiar la cantidad########
          $("#tabla_venta #tbody_venta").find("tr td:first-child").each(function(){
                  if (id_art==$(this).html()) {
                    yaExiste=1;
                    $(this).siblings("td").eq(0).html(parseFloat($(this).siblings("td").eq(0).html())+1);
                    var importe=parseFloat($(this).siblings("td").eq(0).html())*parseFloat($(this).siblings("td").eq(3).html());
                    importe=importe.toFixed(2);
                    $(this).siblings("td").eq(4).html(importe);
                  }
            });//fin each

          if (yaExiste==0) {
            //$("#tabla_venta tbody").append("<tr><td class='d-none'>"+id_art+"</td> <td contenteditable='true'>1</td> <td class=''>"+unidad+"</td><td>"+descripcion+"</td><td contenteditable='true' "+ClaseOcultar+">0</td>  <td class='precio_editable'>"+precio0+"</td> <td class='d-none'>"+existencia+"</td> <td>"+precio0+"</td><td><i class='fa fa-trash-o' onclick='eliminarPRO();' style='color:red'></i></td> </tr>");
            if (true) {//$("#tipo_usuario_log").val()=="admin"
              $("#tabla_venta tbody").append("<tr ><td class='d-none'>"+id_art+"</td> <td contenteditable='true' id='start"+ContTr+"'>1</td> <td class=''>"+unidad+"</td><td>"+descripcion+"</td><td>"+precio0+"</td> <td>"+precio0+"</td><td><i class='fa fa-trash-o' onclick='eliminarPRO();' style='color:red'></i></td></tr>");
            ContTr++;
            }
            //else{
            //   $("#tabla_venta tbody").append("<tr><td class='d-none'>"+id_art+"</td> <td contenteditable='true'>1</td> <td class=''>"+unidad+"</td><td>"+descripcion+"</td><td contenteditable='true' "+ClaseOcultar+">0</td>  <td>"+precio0+"</td> <td class='d-none'>"+existencia+"</td> <td>"+precio0+"</td><td><i class='fa fa-trash-o' onclick='eliminarPRO();' style='color:red'></i></td> </tr>");

            // }//fin else

          }//fin if
          yaExiste=0;
          $('#miModal').modal('hide');
           $("#findProducto").val("");
          $("#findProducto").focus();
        //agregar total
        total=0;
        $("#tabla_venta  #tbody_venta").find("tr td:nth-child(6)").each(function(){
                total+=parseFloat($(this).html());              
            });
            total=total.toFixed(2);
        $("#totalpagar").html(total);
      }//fin if

  }///fin funccion


//###################al precionar enter en el producto buscado#######################
  $('#tb_busqueda ').on('keydown','tr',function(e){
    if (e.which==13) {
       if (clickImg!=1) { //si no se selecciona en el td de la imagen 
          var id_art=$(this).find("td").eq(0).html();
          var unidad=$(this).find("td").eq(2).html();
          var descripcion=$(this).find("td").eq(3).html();
          var precio0=$(this).find("td").eq(4).html();
          var existencia=$(this).find("td").eq(5).html();

          seleccionarProducto(id_art,unidad,descripcion,precio0,existencia);
           
      } //fin click deferente a 1
        clickImg=0;
    }
  });




//++++++++++++++++++++agrega el producto a la tabla de venta al acerle click.
  $('#tb_busqueda ').on("click","tr", function(){
    if (clickImg!=1) {
        var id_art=$(this).find("td").eq(0).html();
        var unidad=$(this).find("td").eq(2).html();
        var descripcion=$(this).find("td").eq(3).html();
        var precio0=$(this).find("td").eq(4).html();
        var existencia=$(this).find("td").eq(5).html();

        seleccionarProducto(id_art,unidad,descripcion,precio0,existencia);

    } //fin click deferente a 1
      clickImg=0;
    }); //fin onclick 


//%%%%%%%%%%%%%%%%AGREGAR POR CODIGO DE BARRAS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  var cant=0; var yaExiste=0;
    $("#findProducto").keydown(function(e){ 
      var producto=$("#findProducto").val();
      if (producto.length>3) {
        if(e.which == 13){ 
           $("#tabla_busqueda tbody").html("");
             //$('#miModal').modal('show');
             
            $.ajax({
              url:"{{route('buscarXcodigo')}}",
              type:'POST',
              dataType:'json',
              data:{ producto:producto}
                }).done(function(e){
                  // alert(e[0].clave);

              ////#############para validar por si ya existe solo cambiar la cantidad########
                    $("#tabla_venta #tbody_venta").find("tr td:first-child").each(function(){
                      if (e[0].id==$(this).html()) {
                        yaExiste=1;
                        $(this).siblings("td").eq(0).html(parseFloat($(this).siblings("td").eq(0).html())+1);
                        var importe=parseFloat($(this).siblings("td").eq(0).html())*parseFloat($(this).siblings("td").eq(3).html());
                        importe=importe.toFixed(2);
                        $(this).siblings("td").eq(4).html(importe);
                      }
              });//fin each
                    
            if (yaExiste==0) {
              if (true) {//$("#tipo_usuario_log").val()=="admin"
                $("#tabla_venta tbody").append("<tr><td class='d-none'>"+e[0].id+"</td><td contenteditable='true'>1</td>  <td class=''>"+e[0].unidad+"</td> <td>"+e[0].descripcion_articulo+"</td> <td>"+e[0].precio_venta+"</td> <td>"+e[0].precio_venta+"</td><td><i class='fa fa-trash-o' onclick='eliminarPRO();' style='color:red'></i></td></tr>");

                  }
                  // else{
                  //   $("#tabla_venta tbody").append("<tr><td class='d-none'>"+e.id_art[x]+"</td><td contenteditable='true'>1</td>  <td class=''>"+e.unidad[x]+"</td> <td>"+e.nombre[x]+"</td><td contenteditable='true' "+ClaseOcultar+">0</td>  <td >"+e.precio[x]+"</td> <td class='d-none'>"+e.existencia[x]+"</td> <td>"+e.precio[x]+"</td><td><i class='fa fa-trash-o' onclick='eliminarPRO();' style='color:red'></i></td></tr>");

                  // }
                }
                  yaExiste=0;
                    total=0;
                    $("#tabla_venta #tbody_venta").find("tr td:nth-child(6)").each(function(){
                    total+=parseFloat($(this).html());
                    });
                    total=total.toFixed(2);
                    $("#totalpagar").html(total);
                    $("#findProducto").val('');

                  
                });//fin done   
                $("#tabla_busqueda").focus();  
           } 
           if(e.which == 40){ 
           buscarPro();
           }//fin e.which==40  
      }       
     });//fin keydown
    $("#btn_buscar").click(function(){
      var producto=$("#findProducto").val();
      if (producto.length>3) {
        buscarPro();
      }else{return false;}
    });
//#############################AL CAMBIAR LA CANTIDAD#####################
  $('#tabla_venta tbody ').on("keyup","tr td:nth-child(2)", function(e){
    if (e.keyCode==40) {
      e.preventDefault();
      // $(".moveTr:focus").parent().next().focus();
    }if (e.keyCode==38) {
      e.preventDefault();
      // $(".moveTr:focus").parent().prev().focus();
    }
      var cantidad=$(this).html();
      var precio=$(this).siblings("td").eq(3).html();
      var importeEnd=cantidad*precio;
      importeEnd=importeEnd.toFixed(2);
      $(this).siblings("td").eq(4).html(importeEnd);
      //agregar total
        total=0;
        $("#tabla_venta tbody").find("tr td:nth-child(6)").each(function(){
              total+=parseFloat($(this).html());
          });
          total=total.toFixed(2);
        $("#totalpagar").html(total);
      });
//#################BLOQUEAR ENTER EN UN TD########################
  $('#tabla_venta tbody ').bind("keypress","tr td", function(e){
    if (e.which == 13) {
      $("#findProducto").focus();
      return false;

    }
  });
//##############ELIMINAR PRODUCTOS DE LA TABLA#####################
  var eliminarPRO=function(){
    $("#tabla_venta tbody").on("click","tr td:last-child",function(){
      var a=$(this).parent();
      //alert(a.html());
      $(a).remove();
      total=0;
        $("#tabla_venta  #tbody_venta").find("tr td:nth-child(6)").each(function(){
                total+=parseFloat($(this).html());              
            });
            total=total.toFixed(2);
        $("#totalpagar").html(total);
    });
      
    }
//#################OBTENER FECHA Y HORA#########################
  //funcion obteniendo fecha
  function fecha(){
    var fecha = new Date();
    var fechahoy=(fecha.getDate()+"-"+(fecha.getMonth()+1)+"-"+fecha.getFullYear());
    return fechahoy;
  }
  function hora(){
    var fecha = new Date();
    var horahoy=(fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds());
    return horahoy;
  }
  $("#fecha").text(fecha());
  setInterval(function(){ $("#hora").text(hora());}, 1000);
//################################################################ 

</script>
@endsection
