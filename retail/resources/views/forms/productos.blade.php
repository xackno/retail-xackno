@extends('layouts.app')

@section('content')


	<div class="container" style="position: relative;width: 120%">
	    <div class="row justify-content-center">
	        <div class="col-md-12">
	        	@if(Session('message'))
	            <div class="alert alert-success alert-dismissible fade show">
						<h3>{{ Session('message') }}</h3>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						</button>
					</div>
	            @endif
	        	@if(session('success'))
					<div class="alert alert-success alert-dismissible fade show">
						<h3>{{session('success')}}</h3>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif
				@if(session('error'))
					<div class="alert alert-danger alert-dismissible fade show">
						<h3>{{session('error')}}</h3>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif
	            <div class="card" style="width: 115%">
	            	<div class="card-header">
	            		<h4>Registrar</h4>
	            		<button class="btn btn-success" id="btn_nuevo-productos"><i class="app-menu__icon  fa fa-plus"></i>Productos </button>
	            		<button class="btn btn-warning" id="btn_nuevo-provedores"><i class="app-menu__icon  fa fa-truck"></i>Provedores </button>
	            		<button class="btn btn-danger" id="btn_nuevo-categorias"><i class="app-menu__icon  fa fa-code-fork"></i> Categorias </button>
	            		<button class="btn btn-info" id="btn_nuevo-lineas"><i class="app-menu__icon  fa        fa-tags"></i> Lineas</button>
	            		<button class="btn btn-primary" id="btn_nuevo-marcas"><i class="app-menu__icon  fa fa-google-wallet"></i> Marcas</button>
	            		<button class="btn btn-success" id="Prod_for_excel"><i class="app-menu__icon  fa fa-file-excel-o"></i> Excel</button>
	            	
	            	</div>
	            <div class="card-body">
	              	
	              	<div class="row">
	              		<div class="col">
	              			<h1>Productos</h1>
	              		</div>
	              		<div class="col">
	              			
	              		</div>
	              		<div class="col-4">
	              			 <form class="form-inline" action="{{route('productos.search')}}" method="GET">
							    <input class="form-control mr-sm-2" type="search" name="busqueda" placeholder="Buscar..." aria-label="Buscar">
							    <button class="btn btn-success" type="submit"><i class="fa fa-search"></i></button>
							  </form>
	              		</div>
	              	</div>
	              	<div class="table-responsive">
					  <table class="table table-striped table-bordered">
					    	<thead class="table-dark ">
				                  <tr>   
				                   <th class="d-none">id</th>
							       <th>Clave</th>
							       <th>Descripción</th>
							       <th>Unidad</th>
							       <th class="d-none">precio_compra</th>
							       <th>Venta</th>
							       <th class="d-none">mayoreo_1</th>
							       <th class="d-none">mayoreo_2</th>
							       <th class="d-none">mayoreo_3</th>
							       <th class="d-none">mayoreo_4</th>
							       <th>SAT</th>
							       <th><i class="fa fa-truck text-warning"></i></th>
							       <th><i class="fa fa-code-fork text-danger"></i></th>
							       <th><i class="fa fa-tags text-info"></i></th>
							       <th><i class="fa fa-google-wallet text-primary"></i></th>
							       <th>Cantidad</th>
							       <th><i class="fa fa-map-marker text-success"></i></th>
							       <th><i class="fa fa-picture-o text-info"></i> </th>
							       <th class="d-none">palabra_clave</th>
							       <th><i class="fa fa-barcode"></i></th>
							       <th><i class="fa fa-gears text-warning"></i></th>
				                  </tr>
				                </thead>
				                <tbody>
				                	@foreach($productos as $producto)
				                	<tr scope="row">
					              	<td>{{$producto->clave}}</td>
					              	<td>{{$producto->descripcion_articulo}}</td>
					              	<td>{{$producto->unidad}}</td>
					              	<td>${{$producto->precio_venta}}</td>
					              	<td>{{$producto->codigo_sat}}</td>
					              	<td>{{$producto->proveedor}}</td>
					              	<td>{{$producto->categoria}}</td>
					              	<td>{{$producto->linea}}</td>
					              	<td>{{$producto->marca}}</td>
					              	<td>{{$producto->existencia}}</td>
					              	<!-- <td>{{$producto->existencia_almacen}}</td> -->
					              	<td>{{$producto->ubicacion_producto}}</td>
					              	<td class="td_imagen">
					              		<span class="d-none">{{$producto->fotos}}</span>	
					              		@if($producto->fotos!="ninguno")
							              	@foreach( explode(',', $producto->fotos) as $imagen)
											    <img src="public/img/{{$imagen}}" class="imgProductos">
											@endforeach
					              		@else
					              			no imagen
					              		@endif
					              	</td>
					              	<td>{{$producto->codigo_barra}}</td>
					              	<td class="td_imgs" style="width: 10%">
					              		<form action="{{ route('productos.destroy',$producto->id) }}" method="post" class="btnAction">
					              			 @csrf
                 							 @method('DELETE')
					              			<button class="btn btn-sm btn-danger" ><i class="fa fa-trash"></i></button>
					              		</form>

					              		<form action="{{route('verProducto') }}" method="post" class="btnAction">
					              			 @csrf
                 							 <input type="hidden" name="id" value="{{$producto->id}}">
					              			<button class="btn btn-sm btn-success" ><i class="fa fa-pencil-square"></i></button>
					              		</form>
					              	</td>
					              	</tr>
					              	@endforeach

				                </tbody>
					  </table>
					  {!!$productos->render() !!}
					</div>
					<style type="text/css">
						.btnAction{width: 48%;display: inline-block;}
					</style>
				
	            </div>
	        </div>

	    </div>
	</div>

<style type="text/css">
	.imgProductos{
		width: 20px;height: auto;
	}

</style>
<!--window modal ######modal altar pro excel################-->
  <div class="modal fullscreen-modal fade" id="pro_excel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light" style="background-color: #009688;" >
      		<h1><i class="fa fa-plus"></i>por Excel  <i class="app-menu__icon  fa fa-file-excel-o"></i> </h1>
      	</div>
        <div class="modal-body">
		    <form action="{{route('productos.import.loadExcel')}}" method="post" enctype="multipart/form-data">
		    	@csrf
	      		<label><b>Altas de productos por excel:</b></label>
	      		<input type="file" name="file" class="form-control">
	      		<br><br>
	      		<!-- <input type="submit" class="btn btn-primary" style="float: right;"> -->
	      		<button class="btn btn-primary" style="float: right;" id="btnimportar">Importar</button>
	      	</form>



        </div>
      </div>
    </div>
  </div>
<!--window modal ######modal provedores################-->
  <div class="modal fullscreen-modal fade" id="modal_imagenes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog " role="document">
      <div class="modal-content" >
      	<div class="modal-header text-dark" style="background-color: #ffc107;" >
      		<h1> imagenes <i class="app-menu__icon  fa fa-picture-o"></i></h1>
      	</div>
        <div class="modal-body" style="text-align: center">

        </div>
      </div>
    </div>
  </div>
<!--window modal ######modal provedores################-->
  <div class="modal fullscreen-modal fade" id="modal_provedores" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-dark" style="background-color: #ffc107;" >
      		<h1><i class="fa fa-plus"></i> Provedor <i class="app-menu__icon  fa fa-truck"></i></h1>
      	</div>
        <div class="modal-body">
        	<form action="{{route('storeProvedor')}}" method="get">
        		<label for="nombres">Nombre</label>
        		<input type="text" name="nombres" class="form-control" placeholder="nombre">
        		<label for="telefono">Telefono</label>
        		<input type="number" name="telefono" class="form-control" placeholder="Telefono con lada">
        		<label for="direccion">Dirección</label>
        		<input type="text" name="direccion" placeholder="Estado, Distrito, municipio, colonia, calle." class="form-control"><br><br>
        		<button type="submit" class="btn btn-success" style="float: right;">Guardar</button>
        	</form>
        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal categoria################-->
  <div class="modal fullscreen-modal fade" id="modal_categorias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light" style="background-color: #dc3545;" >
      		<h1><i class="fa fa-plus"></i> Categoria <i class="app-menu__icon  fa fa-code-fork"></i></h1>
      	</div>
        <div class="modal-body">
        	<form action="{{route('storeCategoria')}}" method="get">
        		<label for="nombres">Nombre</label>
        		<input type="text" name="nombre" class="form-control" placeholder="nombre">
        		<label for="descripcion">Descripción</label>
        		<textarea name="descripcion" id="descripcion" class="form-control"></textarea>
        		<br><br>
        		<button type="submit" class="btn btn-success" style="float: right;">Guardar</button>
        	</form>
        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal linea################-->
  <div class="modal fullscreen-modal fade" id="modal_lineas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light" style="background-color: #17a2b8;" >
      		<h1><i class="fa fa-plus"></i> Linea <i class="app-menu__icon  fa        fa-tags"></i></h1>
      	</div>
        <div class="modal-body">
        	<form action="{{route('storeLinea')}}" method="get">
        		<label for="nombres">Nombre</label>
        		<input type="text" name="nombre" class="form-control" placeholder="nombre">
        		<label for="descripcion">Descripción</label>
        		<textarea name="descripcion" id="descripcion" class="form-control"></textarea>
        		<br><br>
        		<button type="submit" class="btn btn-success" style="float: right;">Guardar</button>
        	</form>
        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal marca################-->
  <div class="modal fullscreen-modal fade" id="modal_marcas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light" style="background-color: #009688;" >
      		<h1><i class="fa fa-plus"></i> Marca <i class="app-menu__icon  fa fa-google-wallet"></i> </h1>
      	</div>
        <div class="modal-body">
        	<form action="{{route('storeMarca')}}" method="get">
        		<label for="nombres">Nombre</label>
        		<input type="text" name="nombre" class="form-control" placeholder="nombre">
        		<label for="descripcion">Descripción</label>
        		<textarea name="descripcion" id="descripcion" class="form-control"></textarea>
        		<br><br>

        		<button type="submit" class="btn btn-success" style="float: right;">Guardar</button>
        	</form>
        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal productos################-->
  <div class="modal fullscreen-modal fade" id="modal_productos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light" style="background-color: #28a745;" >
      		<h1><i class="fa fa-plus"></i> Producto <i class="app-menu__icon fa fa-shopping-basket"></i></h1>
      	</div>
        <div class="modal-body">
        	<div id="div_alert"></div>
        	<form action="{{route('storeProducto')}}" method="GET">
        		<div class="row">

				    <div class="col">
				<label for="clave">Clave</label>
        		<input type="text" name="clave" class="form-control" placeholder="clave ">
        		<label for="descripcion_articulo">descripción del producto</label>
        		<input type="text" name="descripcion_articulo" class="form-control" placeholder="descripción">
				<label for="unidad">unidad</label>
				<select name="unidad" id="unidad" class="form-control">
					<option>Pieza</option>
					<option>Kilogramo</option>
					<option>Litro</option>
					<option>Gramo</option>
					<option>Caja</option>
					<option>Paquete</option>
				</select>
		        		<label for="precio_compra">precio a la compra</label>
		        		<input type="number" name="precio_compra" class="form-control" placeholder="precio a la compra">
						<label for="precio_venta">precio a la venta</label>
		        		<input type="number" name="precio_venta" class="form-control" placeholder="precio a la venta">
				    </div>

				    <div class="col">

				      	<label for="mayoreo_1">Mayoreo 1</label>
		        		<input type="range" name="mayoreo_1" id="mayoreo_1"  class="custom-range  w-75" min="0" max="100" value="5"><span id="span1">5</span><span><b>%</b></span>

		        		<label for="mayoreo_2">Mayoreo 2</label>
		        		<input type="range" name="mayoreo_2" id="mayoreo_2" class="custom-range  w-75" min="0" max="100" value="10"><span id="span2">10</span><span><b>%</b></span>

						<label for="mayoreo_3">Mayoreo 3</label>
		        		<input type="range" name="mayoreo_3" id="mayoreo_3" class="custom-range w-75" min="0" max="100" value="15"><span id="span3">15</span><span><b>%</b></span>

						<label for="mayoreo_4">Mayoreo 4</label>
		        		<input type="range" name="mayoreo_4" id="mayoreo_4" class="custom-range w-75" min="0" max="100" value="20"><span id="span4">20</span><span><b>%</b></span>

		        		<label for="codigo_sat">código SAT</label>
		        		<input type="number" name="codigo_sat" class="form-control" placeholder="Código SAT">

						<label for="proveedor">proveedor</label>
						<select name="proveedor" class="form-control">
							@foreach($provedores as $provedor)
								<option value="{{$provedor->id}}">{{$provedor->nombres}}</option>
							@endforeach
						</select>
				 </div>

				<div class="col">
					<label for="categoria">Categoria</label>
		        		<select name="categoria" class="form-control">
							@foreach($categorias as $categoria)
								<option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
							@endforeach
						</select>
		        		<label for="linea">linea</label>
		        			<select name="linea" class="form-control">
							@foreach($lineas as $linea)
								<option value="{{$linea->id}}">{{$linea->nombre}}</option>
							@endforeach
						</select>
						<label for="marca">Marca</label>
		        			<select name="marca" class="form-control">
							@foreach($marcas as $marca)
								<option value="{{$marca->id}}">{{$marca->nombre}}</option>
							@endforeach
						</select>
						<label for="existencia">existencia</label>
		        		<input type="number" name="existencia" class="form-control" placeholder="existencia en tienda.">

						<label for="local">Local</label>
		        		<select name="local" class="form-control">
		        			@foreach($locales as $local)
		        			<option value="{{$local->id}}">{{$local->nombre}}-{{$local->descripcion}}</option>
		        			@endforeach
		        		</select>
				</div>
				<div class="col">
					<label for="ubicacion_producto">Ubicación del producto</label>
					<input type="text" name="ubicacion_producto" id="ubicacion_producto" class="form-control" readonly="true" style="font-size: 70%">
					<select class="col-3" id="pasillo">
						<option>Pasillo 0</option>
						<option>Pasillo 1</option>
						<option>Pasillo 2</option>
						<option>Pasillo 3</option>
						<option>Pasillo 4</option>
					</select>
					<select class="col-3" id="anden">
						<option>Anden 0</option>
						<option>Anden 1</option>
						<option>Anden 2</option>
						<option>Anden 3</option>
						<option>Anden 4</option>
					</select>
					<select class="col-3" id="vitrina">
						<option>Vitrina 0</option>
						<option>Vitrina 1</option>
						<option>Vitrina 2</option>
						<option>Vitrina 3</option>
						<option>Vitrina 4</option>
					</select>
					<br>

					<label for="palabra_clave">Palabras clave</label>
					<input type="text" name="palabra_clave" class="form-control">

					<label for="codigo_barra">Código de barra</label>
					<input type="number" name="codigo_barra" class="form-control">
					<label for="fotos">Imagenes</label>
					<input type="text" name="fotos" id="fotos" class="form-control" readonly="true" value="ninguno">

				</div>
			</div>
        		
		       	<br><br>
	        	<button type="submit" class="btn btn-success" id="guardarProducto" style="float: right;">Guardar</button>
        	</form>

        	<form enctype="multipart/form-data" id="formuploadajax" method="post">
        		<label for="imagen"><b>Subir imagen:</b></label>
			  <input type="file" name="imagen" id="imagen" />
			  <button  type="submit" class="btn btn-primary" id="subirImg">subir</button>
        	</form>
        <script type="text/javascript">
        	
        	
        </script>

		  


        </div>
      </div>
    </div>
  </div>

@endsection

@section('script')
<script type="text/javascript">



	var imagen="";
	var cont=0;

$("#btnimportar").click(function(){
	$("#pro_excel").modal("show");
});

//_______________________________click a  la imagen____________________
	$("table tbody .td_imagen").click(function(){
		// alert($(this).find("span").html());
		var imagenes=$(this).find("span").html();
		imagenes=imagenes.split(",");
		$("#modal_imagenes .modal-body").html("");
		for(var x=0;x<imagenes.length;x++){
			$("#modal_imagenes .modal-body").append("<div style='width:100%'><img src='public/img/"+imagenes[x]+"' style='width:100%;height:auto;'></div>");
		}
		$("#modal_imagenes").modal('show');
	});
//-----------------------------------------------------------------------------
	$("#pasillo,#anden,#vitrina").on('change',function(){
		var ubicacion=$("#pasillo").val()+","+$("#anden").val()+","+$("#vitrina").val();
		$("#ubicacion_producto").val(ubicacion);
	});

	$("#guardarProducto").click(function(){
		cont=0;
	});

	$("#formuploadajax").on("submit", function(e){
	            e.preventDefault();
	            var f = $(this);
	            var formData = new FormData(document.getElementById("formuploadajax"));
	            formData.append("dato", "valor");
	            //formData.append(f.attr("name"), $(this)[0].files[0]);
	            $.ajax({
	                url: "{{url('/uploadImg')}}",
	                type: "post",
	                dataType: "",
	                data: formData,
	                cache: false,
	                contentType: false,
		     processData: false
	            }).done(function(e){
					// alert(e.nombre);
					if (e.status=="success") {
						if (cont==0) {
							imagen=e.nombre;
						}else{
							imagen=imagen+","+e.nombre;
						}
						cont++;
						
						$("#fotos").val(imagen);
						$("#div_alert").html("<div class='alert alert-success' role='alert'>IMAGEN Cargado correctamente.</div>");
						setTimeout(function(){
				        $( "#div_alert").html('');
				        }, 3500);
					}else{
						$("#div_alert").html("<div class='alert alert-danger' role='alert'>Error al cargar.</div>");
						setTimeout(function(){
				        $( "#div_alert").html('');
				        }, 3500);
					}
		}).fail(function (jqXHR, exception) {
	                // Our error logic here
	                console.log(exception);
	            });
	        });

//------------para sliders------------------
	$("#mayoreo_1").change(function(){
		$("#span1").html($(this).val());
	});
	$("#mayoreo_2").change(function(){
		$("#span2").html($(this).val());
	});
	$("#mayoreo_3").change(function(){
		$("#span3").html($(this).val());
	});
	$("#mayoreo_4").change(function(){
		$("#span4").html($(this).val());
	});
//-------------mostrar modales  agregar------------------------------------
	$("#btn_toggle").trigger("click");
	$("#Prod_for_excel").click(function(){
		$("#pro_excel").modal('show');
	});
	$("#btn_nuevo-provedores").click(function(){
		$("#modal_provedores").modal('show');
	});
	$("#btn_nuevo-categorias").click(function(){
		$("#modal_categorias").modal('show');
	});
	$("#btn_nuevo-lineas").click(function(){
		$("#modal_lineas").modal('show');
	});
	$("#btn_nuevo-marcas").click(function(){
		$("#modal_marcas").modal('show');
	});
	$("#btn_nuevo-productos").click(function(){
		$("#modal_productos").modal('show');
	});
//-------------------------------------------------------
</script>
@endsection
