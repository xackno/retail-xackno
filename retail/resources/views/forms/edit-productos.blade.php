@extends('layouts.app')
@section('content')
<div class="container" style="position: relative;width: 70%">
	<div id="div_alert"></div>
		@if(session('success'))
		<div class="alert alert-success alert-dismissible fade show">
			<h3>{{session('success')}}</h3>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif
		@if(session('error'))
		<div class="alert alert-danger alert-dismissible fade show">
			<h3>{{session('error')}}</h3>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif
		@if($msj)
			<div class="alert alert-info alert-dismissible fade show">
			<h3>{{$msj}}</h3>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif

	    <div class="row justify-content-center">
	        <div class="col-md-12">
	        	
	            <div class="card" style="width: 150%;margin-left: -80px">
	            	<div class="card-header" style="background-color:#20c997">
	            		<h4 class="text-light">Producto Id->{{$producto->id}} {{$producto->descripcion_articulo}}</h4>
	            	</div>
	            	<div class="card-body">

	            	<form action="{{route('actualizar')}}" method="POST">
  						@csrf
	            		<input type="hidden" name="id" value="{{$producto->id}}">
	            	<div class="row">
	            		<div class="col-3">
	            			<label>Clave</label>
	            			<input type="number" name="clave" value="{{$producto->clave}}" class="form-control">

	            			<label>Descripción</label>
	            			<input type="text" name="descripcion_articulo" value="{{$producto->descripcion_articulo}}" class="form-control">

	            			<label>unidad</label>
	            			<select name="unidad" id="unidad" class="form-control">
	            				<option selected="true">{{$producto->unidad}}</option>
								<option>Pieza</option>
								<option>Kilogramo</option>
								<option>Litro</option>
								<option>Gramo</option>
								<option>Caja</option>
								<option>Paquete</option>
							</select>

							<label for="precio_compra">precio a la compra</label>
			        		<input type="number" name="precio_compra" class="form-control" value="{{$producto->precio_compra}}">

							<label for="precio_venta">precio a la venta</label>
			        		<input type="number" name="precio_venta" class="form-control" value="{{$producto->precio_venta}}">
	            		</div>

	            		<div class="col-3">
	            			<label for="mayoreo_1">Mayoreo 1</label>
		        		<input type="range" name="mayoreo_1" id="mayoreo_1"  class="custom-range  w-75" min="0" max="100" value="{{$producto->mayoreo_1}}"><span id="span1">{{$producto->mayoreo_1}}</span><span><b>%</b></span>

		        		<label for="mayoreo_2">Mayoreo 2</label>
		        		<input type="range" name="mayoreo_2" id="mayoreo_2" class="custom-range  w-75" min="0" max="100" value="{{$producto->mayoreo_2}}"><span id="span2">{{$producto->mayoreo_2}}</span><span><b>%</b></span>

						<label for="mayoreo_3">Mayoreo 3</label>
		        		<input type="range" name="mayoreo_3" id="mayoreo_3" class="custom-range w-75" min="0" max="100" value="{{$producto->mayoreo_3}}"><span id="span3">{{$producto->mayoreo_3}}</span><span><b>%</b></span>

						<label for="mayoreo_4">Mayoreo 4</label>
		        		<input type="range" name="mayoreo_4" id="mayoreo_4" class="custom-range w-75" min="0" max="100" value="{{$producto->mayoreo_4}}"><span id="span4">{{$producto->mayoreo_4}}</span><span><b>%</b></span>

		        		<label for="codigo_sat">código SAT</label>
		        		<input type="number" name="codigo_sat" class="form-control" placeholder="Código SAT" value="{{$producto->codigo_sat}}">

						<label for="proveedor">proveedor</label>
						<select name="proveedor" class="form-control">
							@foreach($provedores as $provedor)
								<option value="{{$provedor->id}}">{{$provedor->nombres}}</option>
							@endforeach
						</select>
	            		</div>

	            	<div class="col">
						<label for="categoria">Categoria</label>
		        		<select name="categoria" class="form-control">
		        			@foreach($categorias as $categoria)
		        				@if($producto->categoria==$categoria->id)
								<option value="{{$producto->categoria}}">{{$categoria->nombre}}</option>
								@endif
							@endforeach
		        			
							@foreach($categorias as $categoria)
								@if($categoria->id != $producto->categoria )
								<option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
								@endif
							@endforeach
						</select>
		        		<label for="linea">linea</label>
		        			<select name="linea" class="form-control">
							@foreach($lineas as $linea)
		        				@if($producto->linea==$linea->id)
								<option value="{{$producto->linea}}">{{$linea->nombre}}</option>
								@endif
							@endforeach
		        			
							@foreach($lineas as $linea)
								@if($linea->id != $producto->linea )
								<option value="{{$linea->id}}">{{$linea->nombre}}</option>
								@endif
							@endforeach

						</select>
						<label for="marca">Marca</label>
		        			<select name="marca" class="form-control">
							@foreach($marcas as $marca)
		        				@if($producto->marca==$marca->id)
								<option value="{{$producto->marca}}">{{$marca->nombre}}</option>
								@endif
							@endforeach
		        			
							@foreach($marcas as $marca)
								@if($marca->id != $producto->marca )
								<option value="{{$marca->id}}">{{$marca->nombre}}</option>
								@endif
							@endforeach
						</select>
						<label for="existencia">existencia</label>
		        		<input type="number" name="existencia" class="form-control" value="{{$producto->existencia}}">

						<label for="local">Local</label>
		        		<select name="local" class="form-control">
		        			
		        			@foreach($locales as $local)
		        				@if($producto->local==$local->id)
								<option value="{{$producto->local}}">{{$local->nombre}}-{{$local->descripcion}}</option>
								@endif
							@endforeach
		        			
							@foreach($locales as $local)
								@if($local->id != $producto->local )
								<option value="{{$local->id}}">{{$local->nombre}}-{{$local->descripcion}}</option>
								@endif
							@endforeach
		        		</select>
				</div>
					<div class="col">
					<label for="ubicacion_producto">Ubicación del producto</label>
					<input type="text" name="ubicacion_producto" id="ubicacion_producto" class="form-control" readonly="true" style="font-size: 70%" value="{{$producto->ubicacion_producto}}">
					<select class="col-3" id="pasillo">
						<option>Pasillo 0</option>
						<option>Pasillo 1</option>
						<option>Pasillo 2</option>
						<option>Pasillo 3</option>
						<option>Pasillo 4</option>
					</select>
					<select class="col-3" id="anden">
						<option>Anden 0</option>
						<option>Anden 1</option>
						<option>Anden 2</option>
						<option>Anden 3</option>
						<option>Anden 4</option>
					</select>
					<select class="col-3" id="vitrina">
						<option>Vitrina 0</option>
						<option>Vitrina 1</option>
						<option>Vitrina 2</option>
						<option>Vitrina 3</option>
						<option>Vitrina 4</option>
					</select>
					<br>

					<label for="palabra_clave">Palabras clave</label>
					<input type="text" name="palabra_clave" class="form-control" value="{{$producto->palabra_clave}}" >

					<label for="codigo_barra">Código de barra</label>
					<input type="number" name="codigo_barra" class="form-control" readonly="true" value="{{$producto->codigo_barra}}">
					<label for="fotos">Imagenes</label>
					<input type="text" name="fotos" id="fotos" class="form-control" readonly="" value="{{$producto->fotos}}">

				</div>
				<div class="col" style="overflow-y: scroll;height:320px">
					<div class="card-header"><h4>Imagenes</h4></div>
					<p><?php 
					$fotos=$producto->fotos;
					$fotos = explode(",", $fotos);
					foreach ($fotos as $foto) {
						?>
						<span class="text-success">{{$foto}}</span><br>
						<img src="public/img/{{$foto}}" style="width: 50%"><br>
						<?php
					}
					
					?></p>
					
				</div>
	        	</div>
	        <br><br>
	        <button type="submit" class="btn btn-success" id="guardarProducto" style="float: right;">Guardar</button>

	    </form>
	    <form enctype="multipart/form-data" id="formuploadajax" method="post">
        		<label for="imagen"><b>Subir imagen:</b></label>
			  <input type="file" name="imagen" id="imagen" />
			  <button  type="submit" class="btn btn-primary" id="subirImg">subir</button>
        	</form>
	            </div>
	        </div> 
	            <!-- fin card -->
	        </div>

	    </div>
	</div>
@endsection
@section('script')
<script type="text/javascript">
	var imagen="";
	var cont=0;

//-----------------------------------------------------------------------------
	$("#formuploadajax").on("submit", function(e){
	            e.preventDefault();
	            var f = $(this);
	            var formData = new FormData(document.getElementById("formuploadajax"));
	            formData.append("dato", "valor");
	            //formData.append(f.attr("name"), $(this)[0].files[0]);
	            $.ajax({
	                url: "{{url('/uploadImg')}}",
	                type: "post",
	                dataType: "",
	                data: formData,
	                cache: false,
	                contentType: false,
		     processData: false
	            }).done(function(e){
					// alert(e.nombre);
					if (e.status=="success") {
						if (cont==0) {
							if ($("#fotos").val()=="ninguno") {
								imagen=e.nombre;
							}else{
								imagen=$("#fotos").val()+","+e.nombre;
							}
							
						}else{
							imagen=imagen+","+e.nombre;
						}
						cont++;
						
						$("#fotos").val(imagen);
						$("#div_alert").html("<div class='alert alert-success' role='alert'>IMAGEN Cargado correctamente.</div>");
						setTimeout(function(){
				        $( "#div_alert").html('');
				        }, 3500);
					}else{
						$("#div_alert").html("<div class='alert alert-danger' role='alert'>Error al cargar.</div>");
						setTimeout(function(){
				        $( "#div_alert").html('');
				        }, 3500);
					}
		}).fail(function (jqXHR, exception) {
	                // Our error logic here
	                console.log(exception);
	            });
	        });



$("#guardarProducto").click(function(){
		cont=0;
	});


//------------para sliders------------------
	$("#mayoreo_1").change(function(){
		$("#span1").html($(this).val());
	});
	$("#mayoreo_2").change(function(){
		$("#span2").html($(this).val());
	});
	$("#mayoreo_3").change(function(){
		$("#span3").html($(this).val());
	});
	$("#mayoreo_4").change(function(){
		$("#span4").html($(this).val());
	});
	$("#pasillo,#anden,#vitrina").on('change',function(){
		var ubicacion=$("#pasillo").val()+","+$("#anden").val()+","+$("#vitrina").val();
		$("#ubicacion_producto").val(ubicacion);
	});
//-------------mostrar modales  agregar------------------------------------
	$("#btn_toggle").trigger("click");
//-------------------------------------------------------
</script>
@endsection