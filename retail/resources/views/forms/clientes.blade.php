@extends('layouts.app')
@section('content')
<div class="container" style="position: relative;width: 70%">
	<div id="div_alert"></div>
		@if(session('success'))
	<div class="alert alert-success alert-dismissible fade show">
		<h3>{{session('success')}}</h3>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    <span aria-hidden="true">&times;</span>
		</button>
	</div>
		@endif
		@if(session('error'))
	<div class="alert alert-danger alert-dismissible fade show">
		<h3>{{session('error')}}</h3>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    <span aria-hidden="true">&times;</span>
		</button>
	</div>
	@endif

	<div class="row justify-content-center">
	    <div class="col-md-12">
		    <div class="card" style="width: 150%;margin-left: -80px">
		       	<div class="card-header" style="background-color:#007bff">
		        		<h4 class="text-light">Clientes <i class="fa fa-users"></i></h4>
		        </div>
	        	<div class="card-body">
	        		<ul class="app-breadcrumb breadcrumb" class="text-dark" style="float: right;color: white">
	                  <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
	                  <li class="breadcrumb-item"><a href="{{route('clientes.index')}}" class="text-dark">clientes</a></li>
	                </ul>
	                

	                <div class="row">
	              		<div class="col">
	              			<button class="btn btn-sm btn-primary" id="agregarCliente"><i class="fa fa-plus"></i> Cliente</button>
	              		</div>
	              		<div class="col">
	              			
	              		</div>
	              		<div class="col-4">
	              			 <form class="form-inline" action="{{route('clientes.search')}}" method="GET">
							    <input class="form-control mr-sm-2" type="search" name="busqueda" placeholder="Buscar..." aria-label="Buscar">
							    <button class="btn btn-success" type="submit"><i class="fa fa-search"></i></button>
							  </form>
	              		</div>
	              		<!-- fin clas row -->
	              	</div>
	              	<br>
	              		<div class="table-responsive" >
					  <table class="table table-striped table-bordered">
					    	<thead class="table-dark ">
				                  <tr>   
					                   <th>Foto</th>
								       <th>Nombre</th>
								       <th>Apellidos</th>
								       <th>Municipio</th>
								       <th>Localidad</th>
								       <th>Calle</th>
								       <th>Telefono</th>
								       <th>Correo</th>
								       <th>Usuario</th>
								       <th>Status</th>
								       <th style="width: 10%"><i class="fa fa-gears"></i></th>
				                  </tr>
				                </thead>
				                <tbody>
				                	@foreach($clientes as $cliente)
				                	<tr scope="row">
					                	<td><img src="public/img/{{$cliente->foto}}" class="imgRedonda"></td>
						              	<td>{{$cliente->nombre}}</td>
						              	<td>{{$cliente->apellidos}}</td>
						              	<td>{{$cliente->municipio}}</td>
						              	<td>{{$cliente->localidad}}</td>
						              	<td>{{$cliente->calle}}</td>
						              	<td>{{$cliente->telefono}}</td>
						              	<td>{{$cliente->correo}}</td>
						              	<td>{{$cliente->usuarioApp}}</td>
						              	<td>{{$cliente->status}}</td>
						              	<td class="td_imgs" >
						              		<form action="{{ route('clientes.destroy',$cliente->id) }}" method="post" class="btnAction">
						              			 @csrf
	                 							 @method('DELETE')
						              			<button class="btn btn-sm btn-danger" ><i class="fa fa-trash"></i></button>
						              		</form>

						              		<form action="{{route('vercliente') }}" method="post" class="btnAction">
						              			 @csrf
	                 							 <input type="hidden" name="id" value="{{$cliente->id}}">
						              			<button class="btn btn-sm btn-success" ><i class="fa fa-pencil-square"></i></button>
						              		</form>
						              	</td>
					              	</tr>
					              	@endforeach

				                </tbody>
					  </table>
					  {!!$clientes->render() !!}
					</div>

<style type="text/css">
	.btnAction{width: 48%;display: inline-block;}
</style>


	              	</div>
	        	</div>

		    </div> <!-- fin card -->
	    </div><!-- fin col-md-12 -->
	</div><!-- fin content-center -->

</div><!-- fin div container -->



<style type="text/css">
	.imgRedonda {
    width:40px;
    height:42px;
    border-radius:140px;
    border:2px solid #666;
}
.imgPerfil{
	width:60px;
    height:62px;
    border-radius:140px;
    border:2px solid #666;
}
</style>









<!--window modal ######modal clientes################-->
  <div class="modal fullscreen-modal fade" id="modal_clientes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light" style="background-color: #28a745;" >
      		<h1><i class="fa fa-plus"></i> Cliente <i class="app-menu__icon fa fa-user"></i></h1>
      		<span id="imgParaPerfil"></span>
      	</div>
        <div class="modal-body">
        	<div id="div_alert2"></div>
        	<form action="{{route('clientes.store')}}" method="POST">
        		@csrf
        		<div class="row">

				    <div class="col">
						<label for="nombre">Nombre</label>
		        		<input type="text" name="nombre" class="form-control" placeholder="Nombre o nombres ">
		        		<label for="apellidos">Apellidos</label>
		        		<input type="text" name="apellidos" class="form-control" placeholder="apellidos">
						<label for="municipio">Municipio</label>
						<input type="text" name="municipio" class="form-control" placeholder="municipio o C.P.">
		        		<label for="localidad">Localidad</label>
		        		<input type="text" name="localidad" class="form-control" placeholder="Localidad">
						<label for="calle">Calle</label>
		        		<input type="text" name="calle" class="form-control" placeholder="Nombre y número">
				    </div>

				    <div class="col">

				      	<label for="telefono">teléfono</label>
		        		<input type="tel" name="telefono" class="form-control" >

		        		<label for="correo">Correo</label>
		        		<input type="email" name="correo" class="form-control">

						<label for="usuarioApp">Usuario Para la App</label>
		        		<input type="text" name="usuarioApp" class="form-control">

						<label for="password">Contraseña</label>
		        		<input type="password" name="password"  class="form-control">

		        		<label for="foto">Foto</label>
		        		<input type="text" name="foto" id="foto" class="form-control" readonly="" value="default.png">
				 </div>

			</div>
        		
		       	<br><br>
	        	<button type="submit" class="btn btn-success" style="float: right;">Guardar</button>
        	</form>

        	<form enctype="multipart/form-data" id="formsubirFoto" method="post">
        		<label for="imagen"><b>Subir foto:</b></label>
			  <input type="file" name="imagen" id="imagen" />
			  <button  type="submit" class="btn btn-primary" id="subirImg">subir</button>
        	</form>
		  


        </div>
      </div>
    </div>
  </div>











@endsection
@section('script')
<script type="text/javascript">
/////-----------------------para subir foto
	$("#formsubirFoto").on("submit", function(e){
	            e.preventDefault();
	            var f = $(this);
	            var formData = new FormData(document.getElementById("formsubirFoto"));
	            formData.append("dato", "valor");
	            //formData.append(f.attr("name"), $(this)[0].files[0]);
	            $.ajax({
	                url: "{{url('/uploadImg')}}",
	                type: "post",
	                dataType: "",
	                data: formData,
	                cache: false,
	                contentType: false,
		     processData: false
	            }).done(function(e){
					// alert(e.nombre);
					if (e.status=="success") {

						$("#foto").val(e.nombre);
						$("#imgParaPerfil").html('<img src="public/img/'+e.nombre+'" class="imgPerfil">');
						$("#div_alert2").html("<div class='alert alert-success' role='alert'>IMAGEN Cargado correctamente.</div>");
						setTimeout(function(){
				        $( "#div_alert2").html('');
				        }, 3500);
					}else{
						$("#div_alert2").html("<div class='alert alert-danger' role='alert'>Error al cargar.</div>");
						setTimeout(function(){
				        $( "#div_alert2").html('');
				        }, 3500);
					}
		}).fail(function (jqXHR, exception) {
	                // Our error logic here
	                console.log(exception);
	            });
	        });
//------------------------------------------------------------------------------------------------------






//------------- minimizar menu------------------------------------
	$("#btn_toggle").trigger("click");
//-------------------------------------------------------

$("#agregarCliente").click(function(){
	$("#modal_clientes").modal('show');
});
</script>
@endsection