@extends('layouts.app')
@section('content')
<div class="container" style="position: relative;width: 70%">
	<div id="div_alert"></div>
		@if(session('success'))
		<div class="alert alert-success alert-dismissible fade show">
			<h3>{{session('success')}}</h3>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif
		@if(session('error'))
		<div class="alert alert-danger alert-dismissible fade show">
			<h3>{{session('error')}}</h3>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif




	    <div class="row justify-content-center">
	        <div class="col-md-12">
	        	
	            <div class="card" style="width: 150%;margin-left: -80px">
	            	<div class="card-header" style="background-color:#20c997">
	            		
	            		<span id="Perfil"><img src="public/img/{{$cliente->foto}}" class="imgPerfil" ></span>
	            		<h4 class="text-light" style="float: right;">Cliente {{$cliente->id}}</h4>
	            	</div>
	            	<div class="card-body">
							<style type="text/css">
							.imgPerfil{
								
								position: absolute;float: right;
								width:60px;
							    height:62px;
							    border-radius:140px;
							    border:2px solid #666;
							}
							</style>
	            		<div id="div_alert2"></div>
			        	<form action="{{route('actualizar-cliente')}}" method="POST">
			        		@csrf
			        		<input type="hidden" name="id" value="{{$cliente->id}}">
			        		<div class="row">

							    <div class="col">
									<label for="nombre">Nombre</label>
					        		<input type="text" name="nombre" class="form-control" value="{{$cliente->nombre}}">
					        		<label for="apellidos">Apellidos</label>
					        		<input type="text" name="apellidos" class="form-control" value="{{$cliente->apellidos}}">
									<label for="municipio">Municipio</label>
									<input type="text" name="municipio" class="form-control" value="{{$cliente->municipio}}">
					        		<label for="localidad">Localidad</label>
					        		<input type="text" name="localidad" class="form-control" value="{{$cliente->localidad}}">
									<label for="calle">Calle</label>
					        		<input type="text" name="calle" class="form-control" value="{{$cliente->calle}}">
							    </div>

							    <div class="col">

							      	<label for="telefono">teléfono</label>
					        		<input type="tel" name="telefono" class="form-control" value="{{$cliente->telefono}}">

					        		<label for="correo">Correo</label>
					        		<input type="email" name="correo" class="form-control" value="{{$cliente->correo}}">

									<label for="usuarioApp">Usuario Para la App</label>
					        		<input type="text" name="usuarioApp" class="form-control" value="{{$cliente->usuarioApp}}">

									<label for="password">Contraseña</label>
					        		<input type="password" name="password"  class="form-control" readonly="" value="{{$cliente->password}}">

					        		<label for="foto">Foto</label>
					        		<input type="text" name="foto" id="foto" class="form-control" readonly="" value="{{$cliente->foto}}">
							 </div>

						</div>
			        		
					       	<br><br>
				        	<button type="submit" class="btn btn-success" style="float: right;">Guardar</button>
			        	</form>

			        	<form enctype="multipart/form-data" id="formsubirFoto" method="post">
			        		<label for="imagen"><b>Subir foto:</b></label>
						  <input type="file" name="imagen" id="imagen" />
						  <button  type="submit" class="btn btn-primary" id="subirImg">subir</button>
			        	</form>
		  
	           		</div>
	        </div> 
	            <!-- fin card -->
	        </div>

	    </div>
	</div>
@endsection
@section('script')
<script type="text/javascript">
	//-----------------------------------------------------------------------------
	$("#formsubirFoto").on("submit", function(e){
	            e.preventDefault();
	            var f = $(this);
	            var formData = new FormData(document.getElementById("formsubirFoto"));
	            formData.append("dato", "valor");
	            //formData.append(f.attr("name"), $(this)[0].files[0]);
	            $.ajax({
	                url: "{{url('/uploadImg')}}",
	                type: "post",
	                dataType: "",
	                data: formData,
	                cache: false,
	                contentType: false,
		     processData: false
	            }).done(function(e){
					// alert(e.nombre);
					if (e.status=="success") {
						$("#foto").val(e.nombre);
						$("#Perfil").html('<img src="public/img/'+e.nombre+'" class="imgPerfil" >');
						$("#div_alert2").html("<div class='alert alert-success' role='alert'>IMAGEN Cargado correctamente.</div>");
						setTimeout(function(){
				        $( "#div_alert2").html('');
				        }, 3500);
					}else{
						$("#div_alert2").html("<div class='alert alert-danger' role='alert'>Error al cargar.</div>");
						setTimeout(function(){
				        $( "#div_alert2").html('');
				        }, 3500);
					}
		}).fail(function (jqXHR, exception) {
	                // Our error logic here
	                console.log(exception);
	            });
	        });


//-------------mostrar modales  agregar------------------------------------
	$("#btn_toggle").trigger("click");
//-------------------------------------------------------
</script>
@endsection