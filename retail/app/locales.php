<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class locales extends Model
{
    protected $fillable = [
        'nombre','dscripcion', 'ubicacion','created_at','updated_at',
    ];
}
