<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clientes extends Model
{
    protected $fillable = [
        'nombre',
        'apellidos',
        'municipio',
        'localidad',
        'calle',
        'telefono',
        'correo',
        'usuarioApp',
        'password',
        'status',
        'foto',
    ];
    public function scopeNombre($query, $nombre){
        if($nombre){
            return $query->orWhere('nombre','LIKE', "%$nombre%");
        }
    }
}
