<?php

namespace App\Imports;

use App\productos;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class productosImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new productos([
            'clave'                 => $row['clave'],
            'descripcion_articulo'  => $row['descripcion'],
            'unidad'                => $row['unidad'],
            'precio_compra'         => $row['precio_de_compra'],
            'precio_venta'          => $row['precio_de_venta'],
            'mayoreo_1'             => $row['mayoreo_1'],
            'mayoreo_2'             => $row['mayoreo_2'],
            'mayoreo_3'             => $row['mayoreo_3'],
            'mayoreo_4'             => $row['mayoreo_4'],
            'codigo_sat'            => $row['codigo_sat'],
            'proveedor'             => $row['provedor'],
            'categoria'             => $row['categoria'],
            'linea'                 => $row['linea'],
            'marca'                 => $row['marca'],
            'existencia'            => $row['existencia'],
            'local'                 => $row['local'],
            'ubicacion_producto'    => $row['ubicacion'],
            'fotos'                 => $row['imagen'],
            'palabra_clave'         => $row['palabras_clave'],
            'codigo_barra'          => $row['codigo_de_barra'],
        ]);
    }
    public function headingRow(): int
    {
        return 1;
    }
}
