<?php

namespace App\Http\Controllers;


use App\provedores;
use App\categorias;
use App\lineas;
use App\marcas;
use App\locales;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\File\UploadedFileSplFileInfo;
use App\Imports\productosImport;
use App\productos;


class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos=productos::orderBy('id','DESC')->paginate(20);
        $provedores=provedores::all();
        $categorias=categorias::all();
        $marcas=marcas::all();
        $lineas=lineas::all();
        $locales=locales::all();
        return view('forms.productos',compact('productos','provedores','categorias','lineas','marcas','locales'));
    }

    public function search(Request $request){

        $busqueda=$request->get('busqueda');

        $productos=productos::orderBy('id','DESC')
        ->Descripcion_articulo($busqueda)
        ->palabra_clave($busqueda)
        ->clave($busqueda)
        ->codigo_barra($busqueda)
        ->paginate(20);

        $provedores=provedores::all();
        $categorias=categorias::all();
        $marcas=marcas::all();
        $lineas=lineas::all();
        $locales=locales::all();
        return view('forms.productos',compact('productos','provedores','categorias','lineas','marcas','locales'));
    }




    public function loadExcel(Request $request){
        $file=$request->file('file');
         ;
         if (Excel::import(new productosimport, $file)) {
             return back()->with('message',"Importación exitosa");
         }else{
            return back()->with('error',"Error al importar verifique el archivo.");
         }
        
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->all()!=null) {
            productos::create($request->all());
            return back()->with('success','Registro Creado correctamente.');
        }else{
            return back()->with('error','Error al guardar.');
        }
    }


    public function verProducto(Request $request){
        $id=$request->input('id');
        $producto=productos::find($id);
        $provedores=provedores::all();
        $categorias=categorias::all();
        $marcas=marcas::all();
        $lineas=lineas::all();
        $locales=locales::all();
        $msj="";
        return view('forms.edit-productos',compact('producto','provedores','categorias','lineas','marcas','locales','msj'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request)
    {
         $id=$request->input('id');
        $producto=productos::find($id);
        
        if ($producto->update($request->all())) {
            $msj="Modificado correctamente";
        }else{
            $msj="Error al modificar";
        }

        $provedores=provedores::all();
        $categorias=categorias::all();
        $marcas=marcas::all();
        $lineas=lineas::all();
        $locales=locales::all();

        return view('forms.edit-productos',compact('producto','provedores','categorias','lineas','marcas','locales','msj'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      // delete
        $producto=productos::find($id);
        $producto->delete();
        return back()->with('success','Eliminado correctamente');
    }
}
