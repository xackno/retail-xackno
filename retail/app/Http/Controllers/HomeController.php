<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\locales;
use App\productos;
use App\provedores;
use App\User;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $productos=productos::all();
        $provedores=provedores::all();
        $user=User::all();
        return view('home',compact('provedores','productos','user'));
    }
     
}
