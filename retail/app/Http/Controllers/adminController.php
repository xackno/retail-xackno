<?php

namespace App\Http\Controllers;
use App\provedores;
use App\categorias;
use App\lineas;
use App\marcas;
use App\locales;
use App\productos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class adminController extends Controller
{




public function buscarProducto(Request $request){
        $busqueda=$request->input('producto');
    $productos=productos::where('descripcion_articulo','LIKE','%'.$busqueda.'%')
        ->orWhere('clave','LIKE','%'.$busqueda.'%')
        ->orWhere('palabra_clave','LIKE','%'.$busqueda.'%')
        ->get();
    
    // $sql = "SELECT * FROM productos WHERE descripcion_articulo LIKE '% ? %' ";
    // $productos=DB::select($sql,array($busqueda));

    // $productos= productos::where('id','=',3)->get();
    // $response = array('status' => 'ok', 'User' => "xackno");
    
    return json_encode($productos);
}

public function buscarXcodigo(Request $request){
    $codigo_barras=$request->input('producto');
    $productos=DB::select('SELECT * FROM productos WHERE codigo_barra=?',[$codigo_barras]);
    // $productos= productos::where('id','=',3)->get();
    // $response = array('status' => 'ok', 'User' => "xackno");
    
    return json_encode($productos);
}



    public function uploadImg(Request $request){

        // $nombre="hola llego";
        // $request->file('imagen')->store('');
        $file = $request->file('imagen');
        if ($file!=null) {
            $nombre = $file->getClientOriginalName();
       \Storage::disk('local')->put($nombre,  \File::get($file));
            $status="success";
        }else{
            $status="fail";
        }
       
    
     return $response = array('nombre' => $nombre,'status'=>$status);
    }

    public function register()
    {
        $locales=locales::all();
        return view('auth.register',compact('locales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createProvedor(Request $request)
    {
        if ($request['nombres'] == !null && $request['telefono'] ==!null && $request['direccion']==!null) {
            $this->validate($request,[ 'nombres'=>'required', 'telefono'=>'required','direccion'=>'required']);
            provedores::create($request->all());
            return back()->with('success','Registro Creado correctamente.');
        }else{
            return back()->with('error','Error al guardar.');
        }
    
    }
    public function createCategoria(Request $request)
    {
       if ($request['nombre']==!null && $request['descripcion']==!null) {
            $this->validate($request,[ 'nombre'=>'required', 'descripcion'=>'required']);
            categorias::create($request->all());
            return back()->with('success','Registro Creado correctamente.');
        }else{
            return back()->with('error','Error al guardar.');
        }
    
    }

    public function createLinea(Request $request)
    {
            if ($request['nombre']==!null && $request['descripcion']==!null) {
            $this->validate($request,[ 'nombre'=>'required', 'descripcion'=>'required']);
            lineas::create($request->all());
            return back()->with('success','Registro Creado correctamente.');
        }else{
            return back()->with('error','Error al guardar.');
        }
    
    }

    public function createMarca(Request $request)
    {
            
        if ($request['nombre']==!null && $request['descripcion']==!null) {
            $this->validate($request,[ 'nombre'=>'required', 'descripcion'=>'required']);
            marcas::create($request->all());
            return back()->with('success','Registro Creado correctamente.');
        }else{
            return back()->with('error','Error al guardar.');
        }
        
    
    }

}
