<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\clientes;
class clientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes=clientes::orderBy('id','DESC')->paginate(20);
        return view('forms.clientes',compact('clientes'));
    }

 public function verCliente(Request $request){
        $id=$request->input('id');
        $cliente=clientes::find($id);

        return view('forms.edit-clientes',compact('cliente'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
  public function actualizar(Request $request)
    {
         $id=$request->input('id');
        $cliente=clientes::find($id);
        
        if ($cliente->update($request->all())) {
            $msj="Modificado correctamente";
        }else{
            $msj="Error al modificar";
        }
       return redirect('/clientes')->withInput()->with("success","$msj");
    }


        public function search(Request $request){

        $busqueda=$request->get('busqueda');
        $clientes=clientes::orderBy('id','DESC')
        ->nombre($busqueda)
        ->paginate(20);

        return view('forms.clientes',compact('clientes'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->all()!=null) {
            clientes::create($request->all());
            return back()->with('success','Registro Creado correctamente.');
        }else{
            return back()->with('error','Error al guardar.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $cliente=clientes::find($id);
         $cliente->delete();
        return back()->with('success','Eliminado correctamente.');
    }
}
