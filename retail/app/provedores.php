<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class provedores extends Model
{
    protected $fillable = [
        'id',
        'nombres',
        'telefono',
        'direccion',
        'created_at',
        'updated_at'
    ];
}
