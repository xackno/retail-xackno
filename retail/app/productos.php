<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productos extends Model
{
    //
    protected $fillable = [
        'clave',
        'descripcion_articulo',
        'unidad',
        'precio_compra',
        'precio_venta',
        'mayoreo_1',
        'mayoreo_2',
        'mayoreo_3',
        'mayoreo_4',
        'codigo_sat',
        'proveedor',
        'categoria',
        'linea',
        'marca',
        'existencia',
        'local',
        'ubicacion_producto',
        'fotos',
        'palabra_clave',
        'url',
        'codigo_barra',
    ];

    public function scopeDescripcion_articulo($query, $descripcion_articulo){
        if($descripcion_articulo){
            return $query->orWhere('descripcion_articulo','LIKE', "%$descripcion_articulo%");
        }
    }
    public function scopePalabra_clave($query, $palabra_clave){
        if($palabra_clave){
            return $query->orWhere('palabra_clave','LIKE', "%$palabra_clave%");
        }
    }
    public function scopeClave($query, $clave){
        if($clave){
            return $query->orWhere('clave','LIKE', "%$clave%");
        }    
    }
    public function scopeCodigo_barra($query, $codigo_barra){
        if($codigo_barra){
            return $query->orWhere('codigo_barra','LIKE', "%$codigo_barra%");
        }    
    }




}
