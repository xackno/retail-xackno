<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class marcas extends Model
{
    protected $fillable = [
        'id',
        'nombre',
        'descripcion',
        'created_at',
        'updated_at'
    ];
}
