<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Clientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',50);
            $table->string('apellidos',50);
            $table->string('municipio',80);
            $table->string('localidad',80);
            $table->string('calle',80);
            $table->string('telefono',20);
            $table->text('correo',50);
            $table->string('usuarioApp',80);
            $table->string('password');
            $table->string('status')->default('disable');
            $table->string('foto')->default('default.png');
            $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
